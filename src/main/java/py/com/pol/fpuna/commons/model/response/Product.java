package py.com.pol.fpuna.commons.model.response;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class Product extends py.com.pol.fpuna.commons.model.request.Product implements Serializable {
    private Integer productId;
}
