package py.com.pol.fpuna.commons.model.response;

import java.io.Serializable;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@Builder
@ToString
public class MessageErrors implements Serializable {
    private Integer codigoRetorno;
    private String mensaje;
}
