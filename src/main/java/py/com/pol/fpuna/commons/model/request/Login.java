package py.com.pol.fpuna.commons.model.request;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Ivan
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Login implements Serializable {
    private String email;
    private String pass;
}
