package py.com.pol.fpuna.commons.model.request;

import java.io.Serializable;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@SuperBuilder
public class Product implements Serializable {
    private String description;
    private BigDecimal price;
    private int qty;
    private String status;
    private Integer categoryId;
}
