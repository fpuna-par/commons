package py.com.pol.fpuna.commons.model.response;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Ivan
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class User implements Serializable {
    private Integer id;
    private String name;
    private String lastname;
    private String email;
    private String loginName;
    private String passwd;
    private int userType;
    private String status;
}
